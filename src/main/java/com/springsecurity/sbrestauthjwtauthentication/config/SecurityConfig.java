package com.springsecurity.sbrestauthjwtauthentication.config;

import com.springsecurity.sbrestauthjwtauthentication.filter.JWTAuthenticationFilter;
import com.springsecurity.sbrestauthjwtauthentication.filter.JWTLoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
            // Không cần xác thực
            .antMatchers("/").permitAll()
            .antMatchers(HttpMethod.POST, "/login").permitAll()
            .antMatchers(HttpMethod.GET, "/login").permitAll()
            // Cần xác thực
            .anyRequest().authenticated()
            .and()
            // Filter 1 - JWTLoginFilter
            .addFilterBefore(new JWTLoginFilter("/login", authenticationManager()), 
            UsernamePasswordAuthenticationFilter.class)
            // Filter 2 - JWTAuthenticationFilter
            .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authenticationManager) throws Exception {
        String password = "@bc123";
        String encodedPassword = this.passwordEncoder().encode(password);
        System.out.println("Encoded password of: " + password + " is: " + encodedPassword);

        InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> manageConfig = authenticationManager.inMemoryAuthentication();
        
        //Tạo người dùng vào lưu vào bộ nhớ
        //Spring auto add role
        UserDetails firstUser = User.withUsername("Dino").password(encodedPassword).roles("USER").build();
        UserDetails secondUser = User.withUsername("Dean").password(encodedPassword).roles("USER").build();

        manageConfig.withUser(firstUser);
        manageConfig.withUser(secondUser);
    }
}
