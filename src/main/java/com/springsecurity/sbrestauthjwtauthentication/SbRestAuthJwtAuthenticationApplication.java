package com.springsecurity.sbrestauthjwtauthentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbRestAuthJwtAuthenticationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbRestAuthJwtAuthenticationApplication.class, args);
	}

}
